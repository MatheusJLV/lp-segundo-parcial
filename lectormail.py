import email
import imaplib
from bs4 import BeautifulSoup

# settings
user = "******@gmail.com"
password = "*************"

# login
mail = imaplib.IMAP4_SSL("imap.gmail.com")
mail.login(user, password)

# select LAST
mail.select("inbox")
# mail.list()
result, data = mail.uid("search", None, "ALL")
# print(data)
dataList = data[0].split()
# print(dataList)
ultimo = dataList[-1]
# print(ultimo)


# bucle fetch
result2, emailData = mail.uid("fetch", ultimo, "(RFC822)")
print(emailData)

rawEmail = emailData[0][1].decode("utf-8")
emailMessage = email.message_from_string(rawEmail)

print(emailMessage)
print(dir(emailMessage))
to_ = emailMessage["To"]
from_ = emailMessage["From"]
subject_ = emailMessage["Subject"]
date_ = emailMessage["Date"]
print(emailMessage.get_payload())
# SegundoBucle
counter = 1
for part in emailMessage.walk():
    if (part.get_content_maintype() == "multipart"):
        continue
    filename = part.get_filename()
    if not filename:
        ext = "html"
        #filename = "msg-part-%08d%" %(counter, ext)
    counter += 1

content_type = part.get_content_type()
print(content_type)
if("plain" in content_type):
    #para leer texto llano
    print(part.get_payload())
elif "html" in content_type:
    html_= part.get_payload()
    soup= BeautifulSoup(html_,"html.parser")
    text=soup.get_text()
    print("beautifull soup")
    print(text)
else:
    print(content_type)